package main

import (
	"log"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"gitlab.com/ziggornif/go-websocket-article/tweet"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func makeListener(tweetChan chan tweet.TweetResponse) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		conn, _ := upgrader.Upgrade(w, r, w.Header())
		for {
			event := <-tweetChan
			if err := conn.WriteJSON(event); err != nil {
				log.Println(err)
			}
		}
	}
}

func main() {
	router := gin.Default()
	router.Use(cors.Default())

	tweetService := tweet.NewTweetService()

	tweetChan := make(chan tweet.TweetResponse)

	eventsListener := makeListener(tweetChan)

	router.GET("/tweets", func(c *gin.Context) {
		tweets := tweetService.ListTweets()
		c.JSON(http.StatusOK, tweets)
	})

	router.POST("/tweets", func(c *gin.Context) {
		var input tweet.TweetRequest
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		tweet := tweetService.CreateTweet(input)

		tweetChan <- tweet

		c.JSON(http.StatusOK, tweet)
	})

	router.POST("/tweets/:id/likes", func(c *gin.Context) {
		tweetID := c.Param("id")
		likeErr := tweetService.LikeTweet(tweetID)
		if likeErr != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": likeErr.Error()})
			return
		}

		c.Status(http.StatusNoContent)
	})

	router.GET("/tweets/ws", func(c *gin.Context) {
		eventsListener(c.Writer, c.Request)
	})

	router.Static("/public", "./public")

	router.LoadHTMLFiles("./public/index.html")
	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})

	router.Run(":8080")
}
