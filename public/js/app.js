import {appendTweets, getTweets, postTweet} from "./tweets-service.js";
import Tweet from './tweet.js';

window.onload = async function () {
  const tweetDom = document.getElementById("tweets");

  const tweets = await getTweets();
  appendTweets(tweetDom, tweets)

  document.getElementById("tweetbtn").addEventListener("click", postTweet)

  const conn = new WebSocket("ws://localhost:8080/tweets/ws");

  conn.onmessage = function (evt) {
    const tweet = JSON.parse(evt.data)
    appendTweets(tweetDom, [new Tweet({
      id: tweet.id,
      author: tweet.author,
      date: tweet.created_at,
      message: tweet.message,
      likes: tweet.likes,
    })]);
  };
};